window.onload = () => {
    document.getElementById("button_click").onclick = (a) =>{

        const input1 = document.createElement("input");
        input1.placeholder = "Введите диаметр круга";
        input1.type = "number"
        input1.id = "input-diametr";
        document.body.append(input1);

        const button1 = document.createElement("button");
        button1.textContent = "Нарисовать";
        button1.id = "button-diametr";
        document.body.append(button1);

        document.getElementById("button-diametr").onclick = (b) =>{

            const block = document.createElement("section");
            block.id = "block-style";
            block.classList.add("block-style-list");
            document.body.append(block);

            let x;
            for(x = 1; x <= 100; x++){
               const circle = document.createElement("div");
               circle.id = "circle-style";
               circle.classList.add("circle-style-click");
               circle.style.borderRadius = `${input1.value}px`;
               circle.style.width = `${input1.value}px`;
               circle.style.height = `${input1.value}px`;
               circle.style.background = `hsl(${Math.floor(Math.random() * 360)}, 100%, 50%)`;
               block.append(circle);

              circle.onclick = (e) => {
                e.target.remove();
              }
        }
    }
}
}
    